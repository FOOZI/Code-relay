//编程语言：c++
//故事主题：枯燥老总爱上扫地玲 。。。

/*
 *
 *  玲旦愿   LingDanYuan
 *
 *  bilibili： 朱一旦的枯燥生活
 *
 */

#include <iostream>
#include <ostream>
#include <string>

using namespace std;

namespace LingDanYuan {                 // 玲旦愿

class Episode{         
public:
	Episode(int num) : Num(num) {}

	friend ostream &operator<<( ostream &output, const Episode &E )
      {
		  output << "=========="<<endl;
		  output << "  玲旦愿  "<<endl;
		  output << "  第"<< E.Num  <<"集"<<endl;
		  output << "=========="<<endl;

		  return output;
      }
protected:
	int Num;
};

class Tricycle{	
	friend ostream &operator<<( ostream &output, const Tricycle &T )
	{
		output << "+-----------------------+" << endl;
		output << "+   旦炒饭   烤冷面     +" << endl;
		output << "+-----------------------+" << endl;
		output << "+        三轮车         +" << endl;
		output << "+-----------------------+" << endl;
		output << "   o              o      " << endl;
        
        return output;
	}
};

class People{
	
public:
	People(string name, int gender) : Name(name), Gender(gender){}

protected:
	string Name;
	int Gender;    // 0 == 女, 1 == 男
};

class Actor : public People{
	public:
		Actor(string name, int gender, \
	       	  string charact,   \
			  string pet_phrase): People(name, gender),\
								  Charact(charact),    \
							      Pet_phrase(pet_phrase)
												  {}

	friend ostream &operator<<( ostream &output, const Actor &A )
	{
		output<<A.Name<<"出场"<<endl;

		return output;
	}
	void say(string str="")
	{
		cout<<Name<<": ";
		if(str == "")
		{
			cout<<Pet_phrase<<endl;
		}
		else
		{
			cout<<str<<endl;
		}
	}

	void think(string str)
	{
		cout<<Name<<": ";
		cout<<str<<endl;
	}

	void act(string act)
	{
		cout<<Name;
		cout<<act<<endl;
	}

	private:
		string Charact;
		string Pet_phrase;
};


// 男主
LingDanYuan::Actor Dan("朱一旦", 1, "劳力越带，责任越大(dai); 内八字", "有钱人的生活就是这么朴实无华且枯燥");
// 女主
LingDanYuan::Actor Ling("马小玲", 0, "", "嫩娘个腿");

}


void LingDanYuan01()
{
	using namespace LingDanYuan;
	

	//
	// 剧情太长，只挑了几个动作和对话
	//

	Episode episoe(1);

	cout<<episoe;

	Tricycle tricycle;
	cout<<tricycle;

	cout<<Dan;          // 旦总骑着三轮车出场

	Dan.say("每个月都会骑着我收购的三轮车，找个随遇而安的地方，卖烤冷面。");

	Actor SanLaizi("三赖子",1,"底层男孩", "");
	cout<<SanLaizi;

	SanLaizi.act("点了个底层套餐");

	Dan.act("(故意)加了个索马里进口海参");
	SanLaizi.act("假装没看见，给了5块钱");

	Dan.act("把烤冷面递给他，并露出劳力士");
	SanLaizi.act("颤抖，深深鞠躬");
	SanLaizi.act("回家后，把烤冷面珍藏起来");

	Actor SanLaiziSunzi("三赖子孙子" ,1, "底层男孩的孙子", "");
	cout<<SanLaiziSunzi;

	SanLaizi.say("年轻时，曾得到过一个戴劳力士的人卖的烤冷面");
	SanLaiziSunzi.act("以后打麻将也敢赢村子儿子了");

	cout<<std::endl;
	Dan.say();
	cout<<std::endl;

	cout<<Ling;
	Ling.say("老板,你这旦炒饭没有蛋也就算了，吃着吃着还吃出一块手表,硌的我牙生疼");
	
	Dan.think("是她了，终于遇见了一个不认识劳力士的女孩");
	Dan.say("你想怎麽办");

	Ling.say("怎么着也得给我50块钱");

	Dan.think("50块就能给她快乐，这样的知足的女孩，现在很难找到了");
	Dan.act("帮女孩去掉嘴边的饭渣");
	Dan.say("嫁给我吧");

	Ling.say("神经病啊，也不看看你是干啥的，赖蛤蟆想吃天鹅肉");

	Dan.think("人生中第一次被瞧不起，这感觉特别好。我一定要得到她");


	cout<<endl;

}	

void LingDanYuan02()
{
  using namespace LingDanYuan;
  
  //
  // 话接LinDanYuan01
  //

  Episode episode(2);

  cout << episode;

  Dan.say("或许这真的就是缘分，而你刚好就是那只天鹅");

  Ling.think("这人怕不是有毛病吧，要完钱得赶紧走！");
  Ling.act("退后并用手指着");
  Ling.say("别东扯西扯的，赶紧给钱！")

  Dan.act("轻轻地用手往下划，示意别着急");
  Dan.say("钱总会有的，但是一旦多了可就枯燥无味了");
  Dan.think("钱啊！～～～枯燥无味");
  Dan.say("何不如我娶你，一同体验这枯燥呢？");

  Ling.act("一脸惊愕！");
  Ling.think("怕不是神经病哟")
  Ling.say("我才不管你枯燥不枯燥，赶紧赔钱走人");

  Dan.act("从裤兜里拿出一张红钞！手上的劳力士在举止之间竟被太阳反射得熠熠生辉");

  cout << endl;
}
     
int main()
{
	LingDanYuan01();

	LingDanYuan02();
    
	// ...
	//
	
	return 0;

}