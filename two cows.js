// 两头牛
//编程语言 js
function animal(){
    let location = new Location(); //地点
    location.add('hillside','On a meadow') //山坡的一片草地上
    location.add('period','20th Century') //时间20世纪
    let bull = new Animal('bull'); //公牛
    let cow = new Animal('cow'); //母牛
    location.current('When bulls and cows are full');//当公牛和母牛吃饱之后
    bull.say('I LOVE YOU');
    cow.say('Be ashamed');
    bull.say('Not shy'+ 'I LOVE YOU');
    location.after('Go home hand in hand'); //之后手拉手一起回家
    delete location; //离开山坡
    let family = new Family('time','at night');//在晚上回到了家
    family.now('The cow suddenly fainted') //母牛突然晕倒在地
    //.....请听下回分解
}