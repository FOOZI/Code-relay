
```
//编程语言：js
//故事主题：太古时期，混沌初开，天地初分，天地孕育而生万物...
class shape{
	constructor(name,type,parent){
		this.name=name;
		this.type=type;
		this.parent=parent;
		this.skills=[];
		this.treasures=[];
	}
	setName(name){
		this.name=name;
	}
	setExperience(period,options){
		let _this=this;
		_this[period]={};
		for(let i in options){
			if(i=='skills'){
				if(typeof options[i] == 'object' && options[i].constructor == Array){
					options[i].forEach(item=>{
						!_this.skills.includes(item)&&_this.skills.push(item);
					});
				}else{
					!_this.skills.includes(options[i])&&_this.skills.push(options[i]);
				}
			}else if(i=='treasures'){
				if(typeof options[i] == 'object' && options[i].constructor == Array){
					options[i].forEach(item=>{
						!_this.treasures.includes(item)&&_this.treasures.push(item);
					});
				}else{
					!_this.treasures.includes(options[i])&&_this.treasures.push(options[i]);
				}
			}else{
				_this[period][i]=options[i];
			}
		}
	}
}
//海外有一国土，名曰傲来国。国近大海，海中有一座名山，唤为花果山。那座山正当顶上，有一块仙石，其石有三丈六尺五寸高，有二丈四尺围圆。四面更无树木遮阴，左右倒有芝兰相衬。盖自开辟以来，每受天真地秀，日精月华，感之既久，遂有灵通之意。内育仙胞，一日迸裂，产一石卵，似圆球样大。因见风，化作一个石猴。
let monkey=new shape('','monkey king','heaven and earth',[]);
//灵台方寸山，斜月三星洞”--拜师学艺
monkey.setName('孙悟空');//取名孙悟空
monkey.setExperience('part1',{title:'拜师学艺',skills:['长生之道','地煞变化术','筋斗云']});
//龙宫寻宝
monkey.setExperience('part2',{title:'龙宫寻宝',treasures:['如意金箍棒','凤翅紫金冠','锁子黄金甲','藕丝步云履']});
//大闹天宫
monkey.setExperience('part3',{title:'大闹天宫',skills:'火眼金睛'});
//西天取经
monkey.setExperience('part4',{title:'西天取经',skills:'成佛'});
```
