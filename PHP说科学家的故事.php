<?php
interface Content{
    public function show();
}

class Description implements Content
{
    public function show()
    {
        var_dump("拉格朗日");
        var_dump("罗尔街旁");
        var_dump("守望柯西的忧伤");                   
    }
}

class Limit implements Content
{
    public function show()
    {
        var_dump("若思想有界");
        var_dump("爱已被迫收敛");
        var_dump("感情在定义域内连续");
    }
}

class Inner implements Content
{
    public function show()
    {
        var_dump("洛必达的终结");
        var_dump("解不开泰勒的心结");
        var_dump("是否还在麦克劳林的彷徨中独自徘徊");                   
    }
}

class Calculus implements Content
{
    public function show()
    {
        var_dump("我们拿生命的定积分");
        var_dump("丈量感情的微积分");
        var_dump("换来青春的不定积分"); 
        var_dump("前方是否可导");
        var_dump("等待一生的莱布尼茨");                        
    }
}

//先构造一个很多语言望尘莫及的"数组"
$wonderful_array = [
        '开场白' => new Description(),//简介
        '极限' => new Limit(),//极限与连续
        '中值定理' => new Inner(),//中值定理
        '微积分' => new Calculus() 
];

foreach($wonderful_array as $chapter=>$content)
{
    var_dump('['.$chapter.']');
    $content->show();
}
