package com.mjm.demo.controller;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author boyi
 */
public class StoryTest {
    /** 承接Java-grandMother*/
    @Test
    public void story() throws ParseException {
        // 交代故事时间，人物，地点
        Story story = new Story();
        story.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-03-24 6:00:00"));
        List<String> localities = new ArrayList<>();
        localities.add(0,"外婆的房子");
        localities.add(1,"厨房");
        BigSister bigSister = new BigSister();
        ManEtaGhost manEtaGhost = new ManEtaGhost();
        List<Person> persons = new ArrayList<Person>();
        persons.add(0,bigSister);
        persons.add(1,manEtaGhost);
        story.setLocalities(localities);
        story.setPersonList(persons);

        // 承接上文
        story.setExtendsStory("大女儿准备毒熊嘎婆，以及熊嘎婆准备吃大女儿"+"+上文地址+"
        +"https://gitee.com/Selected-Activities/Code-relay/blob/430e7919fc732bf2df8c0a57ef1b4a1d2f42a3d6/Java-grandMother.java");

        // 故事开始
        Action action = new Action();
        action.addEvent("小房子一大早火光通明");
        House house = new House();
        house.temperatureRise(7);

        // 熊嘎婆搅拌着锅唱道
        manEtaGhost.singSong("三只兔子, 一起在家吃干巴豆, 一只老兔子在家迷路, 还剩两只兔子" +
                "两只小白兔, 白了又白, 黑黑的夜里, 一只小小白兔跟干巴豆一起塞了牙缝, 还剩一只兔子" +
                "一只小白兔, 抱着咸咸的干巴豆, 自己腌入味, 早早起来去锅里睡觉觉");

        // 新人物咕噜登场
        Gollum gollum = new Gollum();
        persons.add(gollum);
        gollum.rattle("my precious, my precious...... it's calling me...");
        // 咕噜在房子附近徘徊
        house.wander(gollum);

        // 熊嘎婆发现咕噜, 对着外面大声喊道
        manEtaGhost.yelling("哪来叨叨叨的入土小矮子, 远点远点, 别打搅我吃早餐");

        // 躲在门外的大女儿被声音, 吓了一跳撞到门外地上的瓶瓶罐罐
        bigSister.knock("瓶瓶罐罐");
        house.ringOutNoise("ping ping pang lang lang...");
        bigSister.grasp(bigSister.youngerSisterFinger);

        // 熊嘎婆听到门外的声音, 说道
        manEtaGhost.say("小小白兔在锅里好, 快来吃早餐, 小白.....孙女宝贝");

        // 大女儿推开厨房的门
        bigSister.pushDoor(localities.get(1));

        // 熊嘎婆一把抓大女儿过来, 让她坐在餐桌前, 相互进行对话
        manEtaGhost.say("孙女啊, 你可听过亲子丼这鲜活的美味");
        manEtaGhost.pushTo(manEtaGhost.meatOverRice);
        bigSister.say("妹妹呢, 那小吃货肯定知道");
        manEtaGhost.say("妹妹还在睡呢, 你有福气了, 外婆做了这, 快吃, 吃完再叫上妹妹");
        manEtaGhost.singSong("小白兔白了又白, 绝望的苦, 天真的甜, 无奈的咸,入味..." +
                "狼里个铃来，狼里个铃......");

        // 大女儿内心现在是崩溃的, 她知道要先下手为强，不能被牵着走
        bigSister.pushTo(bigSister.youngerSisterFinger);
        bigSister.say("外婆你这么累, 光煮好又没吃, 你要不吃昨天带来的干巴豆, 昨天你可喜欢了");

        // 大女儿闪着她卡姿兰大眼睛，begging
        bigSister.wink(5);
        manEtaGhost.say("你个小白眼狼怎么不说吃亲子丼呢, 贪吃, 这干巴豆还是昨天给你的, 咯咯咯" +
                "快点吃吧, 等下妈妈又说我没给早餐你吃了。。。口误, 快点吃");
        bigSister.say("知道了, 爸爸, 你们演技太差了, 还有换衣服别在我窗口看得见的外面");
        manEtaGhost.laugh("不亏我女儿, 真聪, 肯定是妈妈换咕噜套装给你看见了，不知道为什么她喜欢这么喜欢霍比特");
        bigSister.eat("9494");

        // ....time passed
        story.setSecondTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-03-24 8:00:00"));
        // 熊嘎婆在锅里搅拌这大女儿
        manEtaGhost.stir("还是亲子丼, 鲜美点, 一家人就该完完整整");
        manEtaGhost.throwFinger(bigSister.youngerSisterFinger);
        manEtaGhost.stir("不知道吃完个盖饭,拿着手上的戒指喃喃一个不停" +
                "9494, 和之前卖火柴的一样感谢我呢，外婆吃香啊, 把剩下的也丢进去, 戒指丢了");
        house.ringOutNoise(gollum.ring+"叮叮...当...啪");

        // 咕噜 is comming
        gollum.eat("门口的豌豆, biu, biu");
        ArrayList<Person> housePeople = new ArrayList<>();
        housePeople.add(gollum);
        house.setPerson(housePeople);
        gollum.wink(5);
        gollum.laugh("my precious, I found you, dirty person sleep on floor and" +
                "dirty vomitus, er green vomitus");
        gollum.pickUp(gollum.ring);

        // 不测风云，crow
        localities.add("崖边");
        action.addEvent("咕噜坐着崖边，迷醉的盯着魔戒");
        action.addEvent("乌鸦闻到腥味，在附近徘徊");

        // bling, bling, bling 戒指被乌鸦叮走
        gollum.yelling("bad bird,dammit...");

        // time passed again
        story.setSecondTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-03-26 10:00:00"));

        // 边陲小镇
        localities.add("边陲小镇的河沿边");
        Dragon dragon = new Dragon();
        persons.add(dragon);
        dragon.say("小红, 我发现附近树上一鸟窝有bling的one piece，里面的戒指就是你的了");
        dragon.pushTo("小红");

        // 某城卫: 直到我膝盖中了一箭。。
        dragon.laugh("等我参军回来, 我就娶你, 明天我就拿戒指和准备好的聘礼到你家, brabra...");

        // 续下回
        action.addEvent("晚上, 魔戒出来个老头对着小红说道, 你掉的是金虎头, 还是银狼头");
        dragon.say("李李李, 药罗, 消炎诚不骗窝");

        // .......

    }
    @Data
    public static class House{
        @Value("#{20}")
        private Integer temperature;
        private List<Person> person;
        public void temperatureRise(int riseVaule) {
            temperature += temperature+riseVaule;
        }
        public void wander(Person person) {
            System.out.println(person+"屋子附近徘徊");
        }

        public void ringOutNoise(String noise) {}
    }
    @Data
    public static class Person {
        public void singSong(String song) {
        }

        public void rattle(String rattle) {}

        public void say(String line) {}

        public void pushDoor(String where) {}
        //push到某人前面
        public void pushTo(String something2somebody) {}

        public void laugh(String lines) {
        }

        public void eat(String lines) {
        }
        public void wink(Integer time) {
            System.out.println(time+"卡姿兰大眼睛");
        }

        public void yelling(String lines) {

        }
    }
    @EqualsAndHashCode(callSuper = true)
    @Data
    public static class BigSister extends Person{
        @Value("大女儿")
        private String name;
        @Value("14")
        private String age;
        @Value("poison")
        private String youngerSisterFinger;
        public void knock(String obj){}
        public void grasp(String grasp) {}

    }
    @EqualsAndHashCode(callSuper = true)
    @Data
    public static class Gollum extends Person{
        @Value("咕噜")
        private String name;
        @Value("Hobbit")
        private String race;
        @Value("#{500}")
        private Integer age;
        @Value("魔戒")
        private String ring;

        public void pickUp(String pickUp) {
        }
    }
    @EqualsAndHashCode(callSuper = true)
    @Data
    public static class ManEtaGhost extends Person{
        @Value("熊嘎婆")
        private String name;
        @Value("food")
        private String meatOverRice;

        public void stir(String stir) {
        }

        public void throwFinger(String youngerSisterFinger) {
        }
    }
    @EqualsAndHashCode(callSuper = true)
    @Data
    public static class Dragon extends Person{}

    @Data
    public static class Action {
        public void addEvent(String event) {
            System.out.println("event = " + event);
        }
    }
    @Data
    public static class Story {
        private String id;
        private String name;
        private String extendsStory;
        private Date time;
        private List<String > localities;
        private List<Person> personList;
        private List<Action> eventList;
        private Date secondTime;
    }
}
