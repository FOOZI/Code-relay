/*
 * 编程语言：js
 * 《程序人生》
 * 本故事绝对真实，如有雷同，那也没招。
 */
var me = {
    choice : function (msg,yesEventMsg, cancelEventMsg) { 
        if(confirm(msg)){
            this.event(yesEventMsg);
        }
        else{
            this.event(cancelEventMsg);
        }
      },
    event : function (msg) { console.log(msg) }
};

me.event('1986年的某一天，紫日东升，天降祥瑞');
me.event('一只未来的猿出生了...');

me.event('天赋异禀，10个月学会了直立行走');

me.choice('号称要学习，向妈妈要100块钱买小霸王学习机','妈妈信了，于是。。。足球小将、忍者神龟','妈妈果然不给，沮丧一节课');

me.choice('初三的某一天，突然发现班里最漂亮的妹子似乎喜欢自己，要不要有所表示呢','原来人家只是惊鸿一瞥，害我被兄弟们鄙视','懵懂如我，依然懵懂。。。');

//成长中有意思的事情很多，我起个头
