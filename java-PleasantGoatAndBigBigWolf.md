 //编程语言：java
 //故事主题：喜羊羊与灰太狼

 import java.util.ArrayList;
import java.util.List;

//编程语言：java
//故事主题：喜羊羊与灰太狼
public class PleasantGoatAndBigBigWolf {
    public static void main(String[] args) {
        Story story = new Story();
        // 第一章
        story.setChapter(1);
        // 羊历3513年
        story.setTime("SheepCalendar3513");
        //青青草原
        story.setAddress("QingQingPrairie");
        //动物类型
        List<AnimalType> animalTypes = new ArrayList<>();
        //狼
        AnimalType wolf = new AnimalType();
        wolf.setName("Wolf");
        wolf.setCharacter("stupid");
        animalTypes.add(wolf);
        //羊
        AnimalType sheep = new AnimalType();
        sheep.setName("Sheep");
        sheep.setCharacter("gentle");
        animalTypes.add(sheep);

        List<Animal> animalList = new ArrayList<>();
        //喜羊羊
        Animal plaesantGoat = new Animal();
        plaesantGoat.setName("PlaesantGoat");
        plaesantGoat.setAge("7");
        plaesantGoat.setSex("男");
        plaesantGoat.setStatus("happy");
        animalList.add(plaesantGoat);
        //灰太狼
        Animal bigBigWolf = new Animal();
        bigBigWolf.setName("BigBigWolf");
        bigBigWolf.setAge("38");
        bigBigWolf.setSex("男");
        bigBigWolf.setStatus("fierce");
        animalList.add(bigBigWolf);
        story.setAnimalList(animalList);
        // 喜羊羊说你好，朋友们
        plaesantGoat.setTalk("Hello friends!");
        plaesantGoat.say();
        //灰太狼在暗中偷窥喜羊羊
        bigBigWolf.setAction("peep");
        bigBigWolf.doAction();


    }



}

class AnimalType{
  private String name;
  private String character;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }
};

class Animal{
    private String name;
    private String age;
    private String sex;
    private String status;
    private String talk;
    private String action;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTalk() {
        return talk;
    }

    public void setTalk(String talk) {
        this.talk = talk;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void say(){
        System.out.println(name+"："+talk);
    }

    public void doAction(){
        System.out.println(name+"->"+action);
    }
}
class Story{
    private String time;
    private String address;
    private int chapter;
    private List<Animal> animalList;


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getChapter() {
        System.out.println("第"+chapter+"章");
        return chapter;
    }

    public void setChapter(int chapter) {
        this.chapter = chapter;
    }
    public List<Animal> getAnimalList() {
        return animalList;
    }

    public void setAnimalList(List<Animal> animalList) {
        this.animalList = animalList;
    }
}