
namespace Earth;

class Human
{
    public function __construct()
    {
        $this->birthday = date('Ymd His');
    }

    /**
     * @param $job string 工作
     * @param $salary  int 工资
     */
    public function setJob($job, $salary)
    {
        $this->job    = $job;
        $this->salary = 0; // 没有实际得到工资
    }

    public function isAlive()  // 是否还或者
    {
        return $this->hp > 0;
    }

    public function setGirlFriend($girl)  //女朋友
    {
        $this->girlfriend = $girl;
    }

    public function dreams_achieved()   //梦想是否已经实现
    {
        return $this->all_dreams_achieved;
    }

    public function achieveDreams()
    {    //实现梦想
        $i = 999;
        try {
            if (mt_rand(0, 99) !== $i) {    //概率 百分之一
                throw new \RuntimeException('Fail!');
            }
        } catch (\RuntimeException $e) {
            $this->fail_times++;
        }
        echo 'Winner!!';
    }

    public $name;
    private $birthday;
    private $hp = 0;
    /** @var $all_dreams_achieved bool */
    private $all_dreams_achieved = false;
    private $fail_times = 0;
    private $girlfriend;
}


$someOne       = new Human();
$someOne->name = 'someOne';
$less          = 0.00001;
$someOne->setJob('coder', $less);
$someOne->setGirlFriend(null);
$someOne->achieveDreams();
while ($someOne->isAlive() && !$someOne->dreams_achieved()) {
    $someOne->achieveDreams();
}

$time = strtotime('2020年3月23日');
$someOne->{'提交了辞职,准备回家工作陪老婆,同时静下心来好好学习'}();
$someOne->{'想想项目刚上线，项目越来越好，取消辞职申请加薪'}();
$someOne->{'老婆一天没沟通了，怀孕期间情绪容易不稳定，头疼'}();

//============== 突然出现时空裂缝 =====================
//============== 时空裂缝 =============================
穿越中。。。。($someOne, mt_rand(0,1999));
//======++++++++ 时空裂缝 =============================
//====================== 时空裂缝 =====================

$someOne->location = '?';
$someOne->alive    = '?';
$someOne->name     = '?';
$someOne->time     = '?';


echo "live or die depending on The keyboard in Your hand \n"; /** 生存或是死亡 取决于你手中的键盘 */


if (!function_exists('穿越中。。。。')) {
    function 穿越中。。。。($human, $year)
    {
        $human->time -= $year;
    }
}

