public class Flatterer {
	public static void main(String[] args) {
		/**
		 * 有子曰张三，有子曰李四
		 * 同居一世间，路途不相认
		 * 此子千千万，共侍罗桑女
		 * */
		List<Person> flatterers = Arrays.asList(new Person("张三", Integer.MIN_VALUE), new Person("李四", Integer.MIN_VALUE));
		Person thotGoddess = new Person("罗桑", 0);
		
		/**
		 * 或有尺素书，或有珍馐宴
		 * 或有凌罗衫，或有金玉钗
		 * 罗桑何贞洁，却之恐不恭
		 * */
		flatterers.forEach(f -> {
			f.fawnOn(thotGoddess, "情书", "日记", "电影", "奶茶", "包包", "首饰", "化妆品");
		});
		
		/**
		 * 有男曰某某，风采世无双
		 * 著我凌罗衫，著我金玉钗
		 * 罗桑自有情，惜哉事无常
		 * */
		Person maleGod = new Person("某某", Integer.MAX_VALUE);
		thotGoddess.dressUp("首饰", "化妆品");
		try {
			thotGoddess.acquaintedWith(maleGod);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

                 /**
		 * 惜哉事无常，憾之已注定
                 * 罗桑忽得子，某某拂身去
		 * 幸著凌罗衫，庆余金玉钗
		 * */
		Person baby = new Person("某某之子", Integer.MIN_VALUE);
                flatterers.forEach(f -> {
			thotGoddess.fawnOn(f, baby);
		});
	}
}
@Data
@AllArgsConstructor
class Person{
	private String name;
	private Integer level;
	/**
	 * fawn on with nothing result.
	 * */
	public void fawnOn(Person toPerson, Object...objects) {}
	public Person dressUp(Object...objects) {return this;}
	/**
	 * Try to get acquainted
	 * */
	public void acquaintedWith(Person toPerson) throws ClassNotFoundException {}
}