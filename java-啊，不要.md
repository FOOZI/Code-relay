
// 编程语言: java
// 故事主题: 啊！不要

```
public class No {

    public static void main(String[] args) {
        Persion persion = new Persion();
        persion.setName("小明");
        persion.setAge(3);
        history1(persion);


//        ... 请扩展
    }




//    故事一
//    编写时间：2020-03-25 14:06:00
    private static void history1(Persion persion) {
        System.out.println( persion.getName() + "出生在一个小资家庭，当他长到" + persion.getAge() + "岁的时候" );
        System.out.println( "有一天，他的爸爸和妈妈来到他身边，非常和蔼的给他商量：\" 小明，你也不小了，都是3岁的大人了\"" );
        System.out.println( "爸爸哽噎的说道：\" 是时候送你去少管所了\"" );
        System.out.println(" 小明大声说道：\"啊！不要\"" );
    }
}



class Persion{
    String name;
    Integer age;
//    请扩展

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
```