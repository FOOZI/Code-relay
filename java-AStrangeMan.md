
```
//编程语言：java
//故事主题：这天，一个奇怪的人出现了...

import java.util.ArrayList;

public class StoryTest {
    public static void main(String[] args) {
        Story story = new Story();
        List<Person> personList = new ArrayList();
        story.setTime("night");      //在一个月黑风高的晚上
        story.setAddress("lawn");    //我在草坪上散步

        Person person = new Person();
        personList.add(person);      //突然有个人向我走来

        Person personMan = new Person();
        personMan.setBody("strong");
        personMan.setHigh("1.8m");
        personList.add(personMan);    //那个人身材魁梧，一米八几的样子

        story.setPersonList(personList);

        person.setStatus("scared");  
        person.say("你！你是……");     //我被吓坏了，惊道："你！你是……"

        personMan.say("你还记得那年在大明湖畔一起烤红薯的日子么？"); // 那魁梧大汉从兜里掏出一根烟不紧不慢的点上，猛吸了一口，用略带沧桑的嗓音道："你还记得那年在大明湖畔一起烤红薯的日子么？"。

        person.say("你...你...你难道是......"); // “你...你...你难道是......”,这熟悉的动作,这熟悉的嗓音,不经让我背部阵阵发凉。

        BackgroundMusic backgroundMusic = new Background Music();

        backgroundMusic.start();

        person.hear(" chicken , Chicken you are too beautiful!");
        
        pernson.say("你！你是蔡明！");
        
        personMan.laughing(); //强男大笑了起来
        
        person.say("why laugh?");//我：你笑甚？
        
        personMan.say("looking youer SMS");//强男：瞅瞅你的短信
        
        person.say("enn ...");//不翻译
        
        Phone personPhone = new Phone(person);//量子的无中生机（手机登场）
        
        personPhone.getNowMsm().soundMsm("You have a letter from a lawyer please receive！！！");//手机：您有一封待确认的律师函，请注意查收.
        
        person.shivering();//我颤抖了起来（混合的恐惧，不解，愤怒，无奈，还有一一点欢喜？）
       
        person.say("why Why did you do this to me ?!");//我颤抖的嘶吼道：为什么，为什么要这样对我
        
        personMan.say("oh boy just you said '蔡'");//强男：就是你说了“蔡”
        
        person.say("just'蔡',I no say '虚，续。。。 & 鲲，坤。。。',you can not attack to me");//我：只是“蔡”而已啊，我没有再多说“虚”“鲲”之流。
        
        personMan.say("Now！ boy，you said it！");//强男：现在你已经说了
        
        personMan.feelLoveAttack(person);//强男对我施展了爱的打击
        
        person.noHurry();//我不慌
        person.eatTablet();//我吃口药
        person.avoid();//我闪

        personMan.say("气哭秀！");//强男：可恶
        personMan.say("not end !");//强男：这不是结束
        personMan.theJoJoWorld();//强男JOJO的世界替身技能

        person.say("纳尼？！！");
        story.setTime("night");//突然变天，似乎有什么事情即将发生……
        
        personMan.use("axe");//强男拿起了斧头
        backgroundMusic.stop();//背景音乐停止了
        personMan.feelLoveAttack();//强男忘记加参数了所以砍死了自己
        person.call(110);//我报警了

        Person policeMan = new Person();
        policeMan.setBody("invincible");//警察是无敌的
        personList.add(policeMan);//警察登场
        
        policeMan.say( person.getName() + "，是你杀的" + personMan.getName() + "吧！如实招来！");
        policeman.say("我要想打败你，可容易呢！赶紧如实招来！")
        policeman.say("你不是最怕" + policeMan.skill(get.person.weakness()) + "吗？我早就知道了，束手就擒吧！")
    }
}
..... // 接下来的就看你们的了
 } 
```
