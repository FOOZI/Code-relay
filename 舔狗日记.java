
import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Tolerate;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

/**********
 * 舔狗日记
 * ********
 * 一个舔狗的辛酸史
 * 开局车祸 够惨吧
 * 剩下的交给大家了
 */
public class TgRj {
    private static List<Diary> diaryList = Arrays.asList(
            Diary.builder()
                    .dateTime("2020年3月27日周五下午4：27")
                    .weather("雨")
                    .mood("很伤心")
                    .data("\n"
                            + "今天下雨了，路有点滑\n"
                            + "在想一些东西，走在路上恍然间失了神\n"
                            + "一辆车突然驶来没来的及躲避\n"
                            + "我躺在地上，好痛，要死了吗\n"
                            + "恍惚间我看到车牌号有个9\n"
                            + "是啊她生日9月，还剩175天过生日，该给她买什么礼物好哪\n"
                    ).build()
            //后续日记 在此处加入  撒花 ｡:.ﾟヽ(｡◕‿◕｡)ﾉﾟ.:｡+ﾟ
            //模板在此
//            , Diary.builder()
//                    .dateTime("时间")
//                    .weather("天气")
//                    .mood("心情")
//                    .data("\n"
//                            + "日记内容\n"
//                    ).build()
    );

    public static void main(String[] args) {
        diaryList.forEach(diary -> {
            System.out.println(diary.show() + "\n---缓冲一下慢慢往下看 --- 叮： ๑乛◡乛๑");
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    @Builder
    @Data
    @ToString
    public static class Diary {
        public String dateTime;//时间日期(字符串格式)
        public String weather;//天气
        public String mood;//心情
        public String data;//日记内容

        @Tolerate
        public Diary() {
        }

        String show() {
            return ("\n"
                    + "***********\n"
                    + "*  Start  *\n"
                    + "***********\n" +
                    this.toString() +
                    "——————————————————————————————————————————————————")
                    .replace("TgRj.Diary", "").replace("(", "").replace(")", "");
        }
    }
}
