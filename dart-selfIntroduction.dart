
//编程语言：dart
//故事主题：这个人进行了自我介绍
class SelfIntroduction {
  init() {
    List<Map<String, String>> personList = new List();
    Map<String, String> personMan;
    personMan['name'] = '张嘛哈';       // 我乃是张嘛哈是也，嘛哈比较好听
    personMan['gender'] = '男';         // 男性
    personList.add(personMan);
  }
}
