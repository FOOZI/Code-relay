# 编程语言：python
# 故事主题：时间有声音吗？也许他有...
from abc import ABCMeta, abstractmethod
from time import sleep

class Every(metaclass=ABCMeta):
    def __init__(self):
        self.name = ''
        self.alive = True

    def __str__(self):
        return self.name

    @abstractmethod
    def __len__(self):
        pass

    @abstractmethod
    def talk(self):
        pass

    @abstractmethod
    def done(self, done):
        pass


class Time(Every):
    time = None

    def __new__(cls, *args, **kwargs):
        if cls.time is None:
            cls.time = super().__new__(cls, *args, **kwargs)
        return cls.time

    def __init__(self):
        super(Time, self).__init__()
        self.name = 'time'
        self.timelist = {
            '一天': 86400,
            '一星期': 86400 * 7,
            '一个月': 86400 * 7 * 4,
            '一年': 86400 * 7 * 4 * 12,
            '三年': 86400 * 7 * 4 * 12 * 3,
            '四年': 86400 * 7 * 4 * 12 * 4,
            '九年': 86400 * 7 * 4 * 12 * 9,
            '五年': 86400 * 7 * 4 * 12 * 5,
            '十年': 86400 * 7 * 4 * 12 * 10,
        }
        self.time = 0

    def __len__(self):
        return float('inf')

    def talk(self):
        print(f'{self}: ')

    def done(self, done):
        print(f'{self}: {done}.')

    def passaway(self, long):
        self.time += self.timelist.get(long, 0)
        sleep(self.timelist.get(long, 0) / 10000000)
        print(f'after {long}')

    def time_through(self):
        print(f'时间不止，但是一去不回。时间已经过去了:{self.time}s')
        while True:
            pass


class Person(Every):
    def __init__(self):
        super(Person, self).__init__()
        self.name = 'baby'
        self.father = None
        self.mather = None

    def set_parentes(self, monther, father):
        self.father = monther
        self.mather = father
        return self

    def set_name(self, name):
        self.name = name
        return self

    def __len__(self):
        return float(99)

    def talk(self, talk='', to=None):
        print(f'{self}: {talk} to {to}')

    def make_baby(self, other):
        return Person().set_parentes(self, other)

    def done(self, done):
        print(f'{self}: {done}.')


time = Time()
mother = Person().set_name('mun')
father = Person().set_name('dad')

time.done('time run')
son = mother.make_baby(father)
mother.talk('孩子他爹，你说孩子叫什么名字好？', father)
father.talk('就叫康康吧，希望他能够健健康康的成长...', mother)
son.set_name('康康')

time.passaway('一个月')
mother.talk('我怎么叫呀？叫妈妈，对啦，叫妈妈。这个是爸爸，叫爸爸。', son)

time.passaway('一星期')
mother.talk('孩子他爹，给孩子去买点奶粉，快去快回。', son)

time.passaway('一个月')
mother.talk('孩子他爹，康康发烧了，赶紧去买点退烧药。', son)

time.passaway('三年')
mother.talk('康康今年就要读幼儿园啦，开不开心呀。', son)

time.passaway('一年')
son.talk('妈妈，我想骑自行车。', mother)
mother.talk('学自行车找你爹教你去。', son)

time.passaway('一天')
father.talk('骑车谁不会摔，给我爬起来继续')
son.talk('呜，好痛啊', father)
son.done('一脚踹走自行车，转头离去')
mother.talk('孩子他爹，你怎么这么凶', father)
mother.talk('男子汉大丈夫，这点痛怕的了什么', son)

time.passaway('一星期')
son.done('和父亲没说过话')

time.passaway('一星期')
son.talk('爸爸，你再教我一次骑车吧，我还想学', father)

time.passaway('九年')
mother.talk('康康，多少再吃点吧，你吃这么少怎么行', son)
father.talk('多少再吃点吧', son)
son.talk('你们别烦我，我要准备中考了', mother)

time.passaway('一年')
son.done('中考考完后，摔门而入')
son.talk('呜呜呜...', mother)
father.talk('考的怎么样', son)
mother.talk('考的不怎么好吧，没关系，中考只是一次检验，你还有高考，别灰心。', son)
son.talk('都怪你们早上没叫醒我，我迟到了没考成语文。你们都走开！', mother)

time.passaway('三年')
mother.talk('妈给你报了个补习班，快高考了，你看看吧', son)
mother.talk('大学别考到外省了，太远了妈不好照顾你', son)

time.passaway('一年')
mother.talk('行李都带齐了吗？妈给你打好车，已经在楼下了，赶紧去机场吧。', son)

time.passaway('一年')
mother.talk('澳洲的生活还习惯吗？', son)
son.talk('挺不错的', son)
mother.talk('你啥时候回来？', son)
son.talk('妈，晚点聊，我上课了...', son)
mother.talk('哎，对了你爹他...', son)
son.done('挂了电话，嘟...嘟...嘟...')

time.passaway('四年')
mother.talk('隔壁老李说给你推荐一份工作，挺好的，离家近，是律师，你的专业。', son)
son.talk('行了行了，妈，找工作就不用你操心了。', mother)

time.passaway('十年')
time.done('一天晚上')
mother.talk('儿子，还在加班吗？家里厨房里有点汤，电饭煲的饭我给你热着。', son)
son.talk('好的，妈。代码还有几个Bug，我改完就回去。', mother)

time.passaway('五年')
time.done('一天晚上')
son.talk('妈，今天领导给我升职了。', mother)
mother.talk('好，妈妈今晚给你加菜。', son)
son.talk('不用了，妈。今晚领导请吃饭。', mother)
mother.talk('那明天晚上吧，咱俩母子好久没一起吃饭了。', son)
son.talk('升职之后我调取国外的分公司了，明日就走...', mother)

time.passaway('十年')
mother.talk('儿子，你啥时候回来一趟呀？妈可想你了。', son)
son.talk('妈，今年春节我也不回去了。', mother)
mother.talk('妈能去找你吗，过几年可能就走不动了。', son)
son.talk('别了妈，你就在家呆着吧。', mother)

time.passaway('十年')
mother.talk('儿子，你可算回来了。推妈妈出去外头转转呗？', son)
son.done('推起妈妈的轮椅')
mother.talk('儿呀，转眼间你都这么大了，这世间可真快啊。', son)

# 接下去哦

time.passaway('...')
father = son
mother = Person().set_name('wife')
baby = mother.make_baby(father)

time.time_through()
