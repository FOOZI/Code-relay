# 语言：python
# 主题：小董和小王的爱情故事

# -*- coding: UTF-8 -*-
import time
boy='小董'
print('有一个男孩叫',boy)
girl='小王'
print('有一个女孩叫',girl)


def miss(boy,girl):
    print(boy,'和',girl,'在高中相遇')
    print('他们一起上课')
    print('他们一起玩耍')
    print('他们互相开玩笑')


def love(boy,girl):
    print(boy,'和',girl,'谈恋爱了')
    print('他们一起逛街')
    print('他们一起吃小吃')
    print('他们一起穿情侣装')

def daxue(boy,girl):
    print(boy,'和',girl,'报了同一个大学')
    print('他们不同专业')
    print('他们一起吃食堂')
    print('他们一起去看海')
    print('他们一起去海洋馆')

# 小董和小王在高中相遇
miss(boy,girl)
# 一年半后
time.sleep(1.5)
# 小董和小王谈恋爱了
love(boy,girl)
# 他们大学也是在一起的
daxue(boy,girl)
# 四年后(高中+大学)
time.sleep(4)


# 未完待续
# 大家可以接龙，看看哪一个结局是接近现实的。

print('{}:鱼为什么没有脚'.format(girl))
print('{}:从前女娲娘娘来凡间看了之后，说：“谁愿意把自己的脚献出来呢？把天和地撑开来呢？”牛、马、熊大家都不愿意把自己的脚砍掉。只有鱼说：“他们不肯，就砍我吧！”'.format(boy))
print('{}:然后能'.format(girl))
print('{}:女娲说：“鱼呀，还是你好！念你献脚有功，我把大海赏给你。”'.format(boy))
# 女主是个物质的女孩，你懂的！！！！！

print(f"\n{girl}: 新冠肺炎我们不能出去逛街了怎么办?")
print(f"{boy}: 宅家里呗~~~")
print(f"{girl}: 宅家里做什么呀？")
print(f"{boy}: 撸代码呗!!!")
# 男主原来是个程序员