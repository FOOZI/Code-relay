```
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 编程语言：java
 * 故事主题：砍柴的王小二
 *
 * @create: 2020-03-23 11:18
 **/
public class FirewoodChopperWangXiaoer {

    public static void main(String[] args) {
        Environment environment = new Environment();
        environment.setTime("in olden days"); //古时候,

        Person wangxiaoer = new Person();
        wangxiaoer.setName("王小二");
        wangxiaoer.setJob("firewood");      //有一个砍柴的王小二,
        wangxiaoer.setFamily("very poor");      //家里很穷,
        wangxiaoer.setDescList(new ArrayList<String>() {{
            add("无法去读书");
        }}); // 因此，他无法去读书。

        //这一天他正在山上砍柴，不料一脚踏空从山崖上跌到了山底下去了。这王二小在山底下也不知道躺了多久，才醒过来，他没黑在山崖下面往前走，走着走着见到一点亮光
        environment.setTime("one day");             //有一天,
        environment.setAddress("Mountain");
        environment.setPersons(new ArrayList<Person>() {{
            add(wangxiaoer);
        }});                                     //王小二在山上砍柴

        Environment environment2 = new Environment();
        environment2.setTime("one day");
        environment2.setAddress("Mountain bottom");

        environment.getPersons().remove(wangxiaoer);
        environment2.setPersons(new ArrayList<Person>() {{
            add(wangxiaoer);
        }});                                    //不料一脚踏空从山上跌到了山底下去了

        wangxiaoer.setStatus("coma");            //王二小在山底下昏迷了
        Long time = System.currentTimeMillis();
        time += 1000 * 60 * new Random().nextInt(24);

        wangxiaoer.setStatus("wake");           //几个小时后,才醒过来,

        environment2.setDesc("light");   //他看到一点儿亮光,


        //..........未完待续

        wangxiaoer.setStatus("go to");   //他走了过去
        environment3.setAddress("village"); //原来是一个村庄,
        Person villager = new Person();
        Environment environment3 = new Environment(new ArrayList<Person>() {{
            add(villager);
        }});                                //还有村民,
        wangxiaoer.setStatus("surprised"); //王小二大吃一惊,

        Thread.sleep(1000);   // 过了一会,
        wangxiaoer.setstatus("calm"); //王小二逐渐平静,
        Person girl = new Person();
        Environment environment3 = new Environment(new ArrayList<Person>() {{
            add(girl);
        }});             
        wangxiaoer.setStatus("curious"); //他看到一个女孩,内心开始好奇起来,蠢蠢欲动


        wangxiaoer.setStatus("go to");   //他走了过去
        wangxiaoer.say("这里是哪里啊？ 你叫什么名字?");   //王小二说："这里是哪里啊？ 你叫什么名字?"
        girl.setStatus("mysterious smile");     //女孩一脸神秘的笑了笑，
        girl.say("你是几百年来第一个来到这里的幸运儿");  //女孩说："你是几百年来第一个来到这里的幸运儿"
        wangxiaoer.setStatus("doubt");  //王小二一脸疑惑，


        girl.say("我在这里等了几百年了，终于有人来了");
        girl.say("我可以实现你三个愿望。");
        wangxiaoer.say("什么愿望都可以吗?");
        girl.say("是的，任何愿望，只要你能说得出来，就一定会帮你实现");
        wangxiaoer.setStatus("dubious"); //王小二半信半疑


        wangxiaoer.say("我想要花不完的钱！");
        girl.say("好");
        wangxiaoer.setDesc(new ArrayList<String>() {{
            add("手中出现了一个鼓鼓的钱包");
        }});
        girl.say("我给你的那个钱包里面有花不完的钱");
        wangxiaoer.setDesc(new ArrayList<String>() {{
            add("打开钱包果然看到都是钱");
        }});


        //你们加油。。
    }

    //环境
    static class Environment {
        String time;
        String address;
        List<Person> persons;
        String desc;

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public List<Person> getPersons() {
            return persons;
        }

        public void setPersons(List<Person> persons) {
            this.persons = persons;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }
    }

    //人物
    static class Person {
        String name;
        String status;
        String family;
        String job;
        List<String> descList;

        public void say(String desc) {
            System.out.println(desc);
        }

        public List<String> getDescList() {
            return descList;
        }

        public void setDescList(List<String> descList) {
            this.descList = descList;
        }

        public String getJob() {
            return job;
        }

        public void setJob(String job) {
            this.job = job;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getFamily() {
            return family;
        }

        public void setFamily(String family) {
            this.family = family;
        }
    }
}
```