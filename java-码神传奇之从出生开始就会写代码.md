 //编程语言：java
 //故事主题：码神传奇的故事

```
public class MaShenZhuan {

        public static void main(String[]args){
                System.out.println("谁也看不出 也想不到 如今的在翘着二郎腿当保安兼职扫地的顶着一头假发的他");
                System.out.print("曾在谷哥 NASAN 阿里妈妈 千度 各大公司担任过");
                for(int i = 1;i<=26;i++){
                        System.out.print("C"+(char)(64+i)+"O ");
                }
                System.out.println(",只因他写了一首好代码.");
                System.out.println("这传奇故事要从他出生开始讲起 ");
                System.out.println("那是一个凉爽的秋天,出生在10月24日-10:24");
                System.out.println("这位神人从出生开始  就没有了头发");
                
                System.out.println("某日，在门岗小憩的他被一句句争论声吵醒。");
                System.out.println("A: 你会写代码吗？10行代码，9个编译异常！");
                System.out.println("B: 就你会写，for循环里面try catch");
                System.out.println("码神：两位小友，不如上码云，直播写代码，一决高下。另外我这里还有一本秘籍可让两位快速提升编程能力。");
                System.out.println("只见他掏出怀中的一本厚厚的黑书，上面赫然写着几个大字：Java编程思想");
                
                System.out.println("A: 你在教我做事？");
                System.out.println("B: 区区一介保安，也敢在我面前展示Java，你可知我的jdk已经修炼到8重天的境界！");
                System.out.println("码神微微一笑，真气外放");
                System.out.println("A: ..9, 10...14，难道 难道，您是..");
                
        }

}
```